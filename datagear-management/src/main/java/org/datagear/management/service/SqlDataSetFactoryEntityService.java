/*
 * Copyright (c) 2018 datagear.tech. All Rights Reserved.
 */

/**
 * 
 */
package org.datagear.management.service;

import org.datagear.management.domain.SqlDataSetFactoryEntity;

/**
 * {@linkplain SqlDataSetFactoryEntity}业务服务接口。
 * 
 * @author datagear@163.com
 *
 */
public interface SqlDataSetFactoryEntityService extends EntityService<String, SqlDataSetFactoryEntity>
{

}
